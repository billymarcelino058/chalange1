import java.util.Scanner;
public class chalange1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        mainMenu();
        int pil = scanner.nextInt();
        while (pil != 3){
            switch (pil){
                case 1:
                    System.out.println("=======================");
                    System.out.println("1. Luas Persegi");
                    System.out.println("2. Luas Lingkaran");
                    System.out.println("3. Luas Segitiga");
                    System.out.println("4. Luas Persegi Panjang");
                    System.out.println("5. Kembali ke menu sebelumnya ");
                    System.out.println("=======================");
                    System.out.print("Pilihan : ");
                    System.out.println("\n=======================");
                    int pilihan1 = scanner.nextInt();
                    switch (pilihan1){
                        case 1:
                            System.out.print("=================\n");
                            System.out.print("Masukkan Sisi : ");
                            int sisi = scanner.nextInt();
                            System.out.println("Luas Persegi = " + sisi * sisi);
                            break;
                        case 2:
                            System.out.print("=================\n");
                            System.out.print("Masukkan Jari-jari : ");
                            int jari = scanner.nextInt();
                            System.out.println("Luas Lingkaran = " + 3.14 * jari * jari);
                            break;
                        case 3:
                            System.out.print("=================\n");
                            System.out.print("Masukkan Alas : ");
                            int alas = scanner.nextInt();
                            System.out.print("Masukkan Tinggi : ");
                            int tinggi = scanner.nextInt();
                            System.out.println("Luas Segitiga = " + (alas * tinggi) / 2);
                            break;
                        case 4 :
                            System.out.print("=================\n");
                            System.out.print("Masukkan Panjang : ");
                            int panjang = scanner.nextInt();
                            System.out.print("Masukkan Lebar : ");
                            int lebar = scanner.nextInt();
                            System.out.println("Luas Persegi panjang = " + panjang * lebar);
                            break;
                        case 5:
                            mainMenu();
                            pil = scanner.nextInt();
                            break;
                        default:
                            System.out.println("mohon memilih menu yang sesuai");
                    }
                    break;
                case 2:
                    System.out.println("=======================");
                    System.out.println("1. Volume Kubus");
                    System.out.println("2. Volume Balok");
                    System.out.println("3. Volume Tabung");
                    System.out.println("4. Kembali ke menu sebelumnya");
                    System.out.println("=======================");
                    System.out.print("Pilihan : ");
                    System.out.println("\n=======================");
                    int pil2 = scanner.nextInt();
                    switch (pil2){
                        case 1:
                            System.out.print("=================\n");
                            System.out.print("Masukkan sisi 1 : ");
                            int sisi1 = scanner.nextInt();
                            System.out.print("Masukkan sisi 2 : ");
                            int sisi2 = scanner.nextInt();
                            System.out.print("Masukkan sisi 3 : ");
                            int sisi3 = scanner.nextInt();
                            System.out.println("Volume Balok = " + sisi1 * sisi2 * sisi3);
                            break;
                        case 2:
                            System.out.print("=================\n");
                            System.out.print("Masukkan Panjang : ");
                            int panjang = scanner.nextInt();
                            System.out.print("Masukkan Lebar : ");
                            int lebar = scanner.nextInt();
                            System.out.print("Masukkan Tinggi : ");
                            int tinggi = scanner.nextInt();
                            System.out.println("Volume Balok = " + panjang * lebar * tinggi);
                            break;
                        case 3:
                            System.out.print("=================\n");
                            System.out.print("Masukkan Jari-jari : ");
                            int jarijari = scanner.nextInt();
                            System.out.print("Masukkan Tinggi : ");
                            int tinggi1 = scanner.nextInt();
                            System.out.println("Volume Tabung = " + 3.14 * jarijari * jarijari * tinggi1);
                            break;
                        case 4:
                            mainMenu();
                            pil = scanner.nextInt();
                            break;
                        default:
                            System.out.println("mohon memilih menu yang sesuai");
                    }
            }
        }
    }

    public static void mainMenu() {
        System.out.println("=========================================");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("=========================================");
        System.out.println("Menu");
        System.out.println("=========================================");
        System.out.println("1. Bangun Datar");
        System.out.println("2. Bangun Ruang");
        System.out.println("3. Keluar");
        System.out.println("=========================================");
        System.out.print("pilihlah janis bangun yang akan dihitung : ");
        System.out.println("\n=========================================");
    }
}
